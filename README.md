![alt kosowski76.com](kosowski76.png "Kosowski76")
# Meduza

**Opis:** Repository template example - [kosowski76](https://www.kosowski76.com)

## Check list 

1. [x] Introduction
    1. [x] test
    2. [x] opis
2. [x] Installation and Configuration
    1. [x] install
    2. [x] config
3. [x] Git Must Know
    1. [x] Pierwszy project
    2. [x] Git commit - pierwsze zmiany w repo
    3. [x] Markdown - plik README.md
    4. [x] .gitignore
    5. [x] Windows command line git
    6. [x] Git lab branch
    7. [x] Merge Request
    8. [x] Staging i commiting w Web IDE

4. [ ] Git pull, commit, push, Tags, branch, merging and reverting
    1. [x] Git pull i Commit
    2. [x] Używanie tagów
    3. [x] Używanie branches (branch)  

    4. [x] Git push tags  

        git tag  
        git tag -a vX.X -m "[XX-00] Description message"  
           -(git tag -a v4.3 -m "[GLAB-04.04-TAG] Git push tags - for v4.3 ag") 
        git tag -d vX.X 
        git push origin vX.X  
          -(git push origin v4.3)  

    
    5. [x] Merging Branches  

        cat .git/HEAD  
        cat .git/refs/heads/development  
        cat .git/refs/heads/main  

        
    6. [x] Reverting a Commit - odwracanie zmian


    7. [x] Komenda diff


    8. [x] Garbage Collection in Git
        git gc --prune  
        git gc --auto  
        git gonfig gc.pruneexpire "30 days"  


5. [ ] Branch Managment

    1. [x] Force Push for remote repository

        ...  
        git reset --hard origin/master  


    2. [x] Identification branches  

        git branch --merged
        git branch --no-merged
        git branch -r --merged
        git branch --merged HEAD
        git branch --merged 34cb212f  


    3. [x] Usuwanie lokalnego i zdalnego brancha  

        git push origin <local>:<remote>
        git push origin :new_feature 

        git push --delete origin new_feature
        git push -d origin new_feature
        git push -d new_feature

        git push -u origin new_feature
    
        git branch
        git branch -r  


    4. [x] Prune stale branch - usuwanie informacji o śledzonym zdalnym branchu  

        git branch -r  
        git remote prune origin --dry-run  
        git remote prune origin  
        git branch -r  

        git fetch --prune  
        git fetch -p  

        git config --global fetch.prune true  

    
    5. [x] Git log - logi commitów  

        git log --graph
    
        git log --since="10 days ago"
        git log --since="10 days ago" --graph
        git log --stat --graph

        git log --pretty=format:"%h - %an - %ar - %s


    6. [x] Tagging - tworzenie ważnych punktów historii

        git tag --list  
        git tag --list "v*"  

        git show v.0.0

        git tag -ln  

        git push origin v.0.0
        git push origin --tags  







